<?php

/**
 * Created by PhpStorm.
 * User: katya
 * Date: 07.06.16
 * Time: 2:20
 */
class Model_test extends Model_base{

    protected $db;
    private $tableName = 'user_answers';
    public $id = 1;
    public $testId;
    public $answers;
    public $userFIO;
    public $date;

    function __construct($registry) {
        parent::__construct($registry);
        $this->db = $this->registry['db'];
    }

    public function saveModelTest($post) {
        $this->db->save($this->tableName, $post);
    }

    public function readModelTest() {
        $this->db->read($this->tableName, $this->id);
    }

    public function readAllModelTest() {
        $result = $this->db->readAll($this->tableName);

        return $result;
    }
}