<?php

/**
 * Created by PhpStorm.
 * User: katya
 * Date: 07.06.16
 * Time: 23:29
 */
class Model_blog extends Model_base
{
    protected $db;
    private $tableName = 'blog';
    private $commentsTableName = 'comments';
    public $id_post;
    public $text;
    public $theme;
    public $date;
    public $imgPath;

    function __construct($registry) {
        parent::__construct($registry);
        $this->db = $this->registry['db'];
    }

    public function saveComment($data) {
        $this->db->save($this->commentsTableName, $data);
    }

    public function saveModelBlog($post) {
        $this->db->save($this->tableName, $post);
    //    echo "saved!";
    }

    public function readModelBlog() {
        $this->db->read($this->tableName, $this->id);
    }

    public function readAllModelBlog() {
        $posts = $this->db->readAll($this->tableName);
        $comments = $this->db->readAll($this->commentsTableName);
        $result['posts'] = $posts;
        $result['comments'] = $comments;
        return $result;
    }

    public function updatePost($data, $id) {
        $this->db->update($this->tableName, $data, $id);
    }
}