<?php

/**
 * Created by PhpStorm.
 * User: katya
 * Date: 07.06.16
 * Time: 23:29
 */
class Model_visitors extends Model_base
{
    protected $db;
    private $tableName = 'visitors';
    public $id;
    

    function __construct($registry) {
        parent::__construct($registry);
        $this->db = $this->registry['db'];
    }

    public function saveModelVisitors($post) {
        $this->db->save($this->tableName, $post);
        echo "saved!";
    }

    public function readModelVisitors($id) {
        $this->id = $id;
        $result = $this->db->read($this->tableName, $this->id);
        return $result;
    }

    public function readAllModelVisitors() {
        $result = $this->db->readAll($this->tableName);
        return $result;
    }
}