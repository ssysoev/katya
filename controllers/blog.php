<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 29.05.16
 * Time: 13:35
 */

Class Controller_Blog Extends Controller_Base {

    private $model;
    private $array2xml;

    function __construct($registry) {
        parent::__construct($registry);
        require_once "./model/blog.php";
        require_once "./model/array2xml.php";
        $this->model = new Model_blog($registry);
        $this->array2xml = new Array2XML();
    }

    function index() {
        $result = $this->model->readAllModelBlog();
        $this->registry['template']->showBlog('blog', $result);
    }

    public function addComment() {
        if(isset($_POST['data'])) {
            $data = new SimpleXMLElement($_POST['data']);
            $data = (array)$data;
            $data['author'] = $_SESSION['vlogin'];
            $this->model->saveComment($data);
            $data['date'] = date('Y-m-d H:i:s');
            $xml = $this->array2xml->convert($data);
            echo $xml;
        }
    }

}