<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:54
 */

Class Controller_Index Extends Controller_Base {

    function index() {
        session_start();
        print_r($_SESSION);

        $this->registry['template']->show('index');
    }
}