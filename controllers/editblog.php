<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:54
 */

Class Controller_Editblog Extends Controller_Base {

    private $model;

    function __construct($registry) {
        parent::__construct($registry);
        require_once "./model/blog.php";
        $this->model = new Model_blog($registry);

    }

    function index() {
        session_start();
        print_r($_SESSION);

        if (($_SESSION['login'] == 'admin') && ($_SESSION['pswd'] == 'admin')) {
            $result = $this->model->readAllModelBlog();
            $this->registry['template']->showPostsAdmin('editblog', $result['posts']);
        } else {
            $this->registry['template']->show('autor');
        }
    }

    function upload() {
        $post = array(
            'id' => 'NULL',
            'theme' => htmlspecialchars($_POST['theme']),
            'img' => htmlspecialchars($_POST['file']),
            'text' => htmlspecialchars($_POST['message']),
            'date' => date('Y-m-d h:i:s')
        );
        $this->model->saveModelBlog($post);

        $this->registry['template']->show('editblog');
    }

    public function updatePost() {
        $data = file_get_contents('php://input');
        $data = new SimpleXMLElement($data);
        $dataArray = array();
        $dataArray['theme'] = $data->title;
        $dataArray['text'] = $data->text;
        $this->model->updatePost($dataArray, $data->id);
        $result = "<h3>" . $dataArray['theme'] . "</h3>" . $dataArray['text'];
        $result .= '<br><button class="open-modal edit-post" data-modal-id="'.$data->id.'">Редактировать</button>';
        $result .= '<div data-modal-id="'.$data->id.'" class="modal">
                          <div class="modal-content">
                            <div class="modal-body">
                              <span class="close">×</span>
                              <input type="text" data-post-id="'.$data->id.'" value="'.$dataArray['theme'].'"><br>
                              <textarea cols="50" rows="10" data-post-id="'.$data->id.'">'.$dataArray['text'].'</textarea><br>
                              <button class="save" data-post-id="'.$data->id.'">Сохранить</button>
                            </div>
                          </div>
                        </div>';
        echo $result;
    }
}