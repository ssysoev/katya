<?php

/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 23:32
 */
Class Controller_Autor Extends Controller_Base
{
    private $model;

    function __construct($registry) {
        parent::__construct($registry);
        require_once "./model/visitors.php";
        $this->model = new Model_visitors($registry);
    }

    function index()
    {

        $this->registry['template']->show('autor');
    }

    function upload()
    {
        echo"ok";
        $log = htmlspecialchars($_POST['login']);
        $pasw = htmlspecialchars($_POST['pswd']);
        $handle = fopen("data/pswd.csv", 'r');
        $pass = "";
        $login = "";

        session_start();

        if (!$handle) {
            echo "Ошибка при открытии файла" . "<br />";
        }

        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $pass = $data[0];
            $login = $data[1];
        }

        $_SESSION['login'] = $log;
        $_SESSION['pswd'] = $pasw;

        if (($log == $login) && ($pass == $pasw)) {
            $this->registry['template']->show('admin');
        } else {
            echo "Ошибка";
            $this->registry['template']->show('autor');
        }
    }

    function autorizeVisitors()
    {
        session_start();

        $log = htmlspecialchars($_POST['login']);
        $pasw = htmlspecialchars($_POST['pswd']);
        $LOG = false;
        $PSWD = false;

        $result = $this->model->readAllModelVisitors();

        foreach ( $result as $var ) {
            foreach( $var as $key => $value) {
                if(($key == 'login') && ($value == $log)  ) {
                    $LOG = true;
                } elseif ( ($key == 'passwd') && ($value == $pasw)  ) {
                    $PSWD = true;
                }
            }
        }

        if ($LOG && $PSWD) {
            $_SESSION['vlogin'] = $log;
            $_SESSION['vpswd'] = $pasw;
            echo "Welcome, ".$log;
            $this->registry['template']->show('index');
        } else {
            echo "error";
            $this->registry['template']->show('index');
        }
    }
    function unautorizeVisitors() {
        $this->registry['template']->show('index');
        session_start();
        session_destroy();

    }
}