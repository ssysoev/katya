<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 29.05.16
 * Time: 13:45
 */
Class Controller_Testresult Extends Controller_Base {

    public $model;

    function __construct($registry) {
        parent::__construct($registry);
        require_once "./model/test.php";
        $this->model = new Model_test($registry);
    }

    function index() {
        $result = $this->model->readAllModelTest();
        $this->registry['template']->showModel('testresult',$result);
    }
}