<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 23:32
 */

Class Controller_Registration Extends Controller_Base {

    private $model;

    function __construct($registry) {
        parent::__construct($registry);
        require_once "./model/visitors.php";
        $this->model = new Model_visitors($registry);
    }

    function index() {
        $this->registry['template']->show('registration');
    }

    function send() {
        $post = array(
            'login' => htmlspecialchars($_POST['login']),
            'passwd' => htmlspecialchars($_POST['pswd']),
            'fio' => htmlspecialchars($_POST['fio']),
            'email' => htmlspecialchars($_POST['email'])
        );
        
        $this->model->saveModelVisitors($post);
        $this->registry['template']->show('index');
    }

    function checkLogin() {
        if(isset($_GET['login'])) {
            $login = htmlspecialchars(stripcslashes($_GET['login']));
            $isLoginAvailable = $this->model->readModelVisitors($login);
            if(empty($isLoginAvailable)) {
                echo 'available';
            }
        }
    }

}