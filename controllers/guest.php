<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 22:25
 */

Class Controller_Guest Extends Controller_Base {


    function index() {
        $this->registry['template']->show('guest');
    }
    
    function upload(){
        $fio = htmlspecialchars($_POST['fio']);
        $email = htmlspecialchars($_POST['email']);
        $message = htmlspecialchars($_POST['message']);


        $f = fopen("data/guestbook.inc", "a");
        if ($f) {
            fwrite($f, "\n" . $fio . ";" . $email . ";" . $message . ";" . date("d.m.Y"));
        } else echo "Ошибка при открытии файла" . "<br />";
        fclose($f);

        $this->registry['template']->show('guest');
    }
}