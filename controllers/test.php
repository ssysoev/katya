<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 22:25
 */

Class Controller_Test Extends Controller_Base {

    private $model;

    function __construct($registry) {
        parent::__construct($registry);
        require_once "./model/test.php";
        $this->model = new Model_test($registry);
        $this->model->id = 2;
    }

    function index() {
        $this->registry['template']->show('test');
    }

    function send() {
        echo "sended!";
        $post = array(
            'id' => 'NULL',
            'id_test' => 1,
            'answers' => htmlspecialchars($_POST['Q1']) . ',' .
                         htmlspecialchars($_POST['Q2']) . ',' .
                         htmlspecialchars($_POST['Q3']),
            'fio_user' => htmlspecialchars($_POST['fio']),
            'date' => date('Y-m-d h:i:s')
        );
        $this->model->saveModelTest($post);
      //$this->model->readModelTest();

        $this->registry['template']->show('test');
    }

    public function showAll() {
        $this->model->readAllModelTest();
    }

}