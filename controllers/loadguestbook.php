<?php

/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:54
 */
Class Controller_Loadguestbook Extends Controller_Base
{
    function index()
    {
        session_start();
        print_r($_SESSION);
        if (($_SESSION['login'] == 'admin') && ($_SESSION['pswd'] == 'admin')) {
            echo "ok";
            $this->registry['template']->show('loadguestbook');
        } else {
            $this->registry['template']->show('autor');
        }
    }

    function upload()
    {
        $uploadDir = '/home/katya/dev/Web/WEB_LABS/katya/LAB_5/data/';
        $uploadFile = $uploadDir . basename($_FILES['file']['name']);
        move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile);
        $this->registry['template']->show('loadguestbook');
    }
}