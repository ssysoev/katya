function list(type){
	document.write("<"+type+"L>");
	for (var i = 1; i < list.arguments.length; i++) {
		document.write("<li>" + list.arguments[i] + "</li>");
	} 
//	document.write("</" + type + "L>");
}

function showDateAndTime() {
    var time = document.getElementById("time");
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var min = d.getMinutes();
    var hour = d.getHours();

    if (day < 10) day = '0' + day;
    if (month < 10) month = '0' + month;
    if (min < 10) min ='0' + min;
    if (hour < 10) hour ='0' + hour; 
    time.innerHTML= "<h4>" + day + "." + month + "."+ year +"<p>"+ hour + ":"+ min + "</h4>";
}                

function hobbyMenu() {
	var show = document.getElementById('hobbyMenu');
    var subMenu = document.getElementById('subHobbyMenu');
    if(show != null && subMenu != null) {
        show.addEventListener('mouseover', function() {
            subMenu.style.display = 'inline-block';
        }, false);

        show.addEventListener('mouseout', function() {
            subMenu.style.display = 'none';
        }, false);
    }
}

function setCookie(key) {
    var value = document.cookie.match(new RegExp(
        "(?:^|; )" + key.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));

    value = value ? decodeURIComponent(value[1]) : null;
    if (isNaN(value)) {
        value = 1;
    } else {
        var v = parseInt(value);
        value++;
    }
    document.cookie = key + "=" + value;
}

function visitPage(key) {
    if (localStorage.getItem(key) == null) {
        localStorage.setItem(key, 1);
    } else {
        var value = parseInt(localStorage.getItem(key));
        value++;
        localStorage.setItem(key, value);
    }
}
