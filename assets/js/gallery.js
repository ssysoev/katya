$(function() {

    var photoPath = ['assets/img/f1.jpg', 'assets/img/f2.jpg', 'assets/img/f3.jpg', 'assets/img/f4.jpg',
        'assets/img/f5.jpg', 'assets/img/f6.jpg', 'assets/img/f7.jpg', 'assets/img/f8.jpg',
        'assets/img/f9.jpg', 'assets/img/f10.jpg', 'assets/img/f11.jpg', 'assets/img/f12.jpg',
        'assets/img/f13.jpg', 'assets/img/f14.jpg', 'assets/img/f15.jpg'];

    var photoTitles = ['Это я', 'Это я','Это я','Это я',
        'Это я','Это я','Это я','Это я',
        'Это я','Это я','Это я','Это я',
        'Это я','Это я','Это я'];

  $(photoPath).each(function(index, element) {
    $('#gallery').append('<img id="myImg" name = "img" alt = '+index+' src = '+element+' title='+photoTitles[index]+'>');
  });
  var currentPhoto;
    $('img[id=myImg]').on('click', function() {
      $('#myModal').css('display', 'block');
      $('#img01').attr('src', $(this).attr('src'));
      $('#caption').html($(this).attr('title'));
      currentPhoto = $(this).attr('alt');
    });
    $('.close').on('click', function() {
      $('#myModal').css('display', 'none');
    });
    $('.next').on('click', function() {
      if (currentPhoto == $('img[id=myImg]').length) {
        currentPhoto = -1;        
      }
      currentPhoto++;
      // $('#img01').fadeOut(500);
      $('#img01').attr('src', $('img[alt='+currentPhoto+']').attr('src'));
      $('#caption').html($('img[alt='+currentPhoto+']').attr('title'));
    });
    $('.prev').on('click', function() {
      if (currentPhoto == 0) {
        currentPhoto = $('img[id=myImg]').length;        
      }
      currentPhoto--;
      $('#img01').attr('src', $('img[alt='+currentPhoto+']').attr('src'));
      $('#caption').html($('img[alt='+currentPhoto+']').attr('title'));
    });
});
