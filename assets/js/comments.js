/**
 * Created by serge on 10/9/16.
 */
function addIFrame(form) {
    var frame = document.createElement('iframe');
    frame.name = 'asyncFrame';
    frame.style.display = 'none';
    frame.onload = frame.onreadystatechange = function() {
        var response = this.contentWindow.document.body.innerHTML;
        response = $.parseXML(response);
        var data = {
            post_id: $(response).find('post_id').text(),
            date: $(response).find('date').text(),
            author: $(response).find('author').text(),
            comment: $(response).find('comment').text()
        };
        $('button[data-post-id='+data.post_id+']').parent().find('.comment').last().after('<div class="comment">\
        <span>Комментарий написан </span>\
    <span class="date">'+data.date+'</span>\
    <span class="author">Автор: '+data.author+'</span>\
    <p class="comment-text">'+data.comment+'</p>\
    </div>')
    }
    document.body.appendChild(frame);
    form.submit();
    //document.getElementsByName('iframe')[0].remove();
}

(function($) {
    $('button[class=toggle-comment-form]').on('click', function () {
        $(this).siblings('.comment-form').toggle(100);
    });
    $('button[class=add-comment]').on('click', function(e) {
        e.preventDefault();
        var comment = $(this).parent().find('#new-comment-text').val();

        if(comment != '') {
            var dataToSend = '<comment_block>'+
                    '<post_id>'+$(this).attr('data-post-id')+'</post_id>'+
                    '<comment>'+comment+'</comment>'+
                '</comment_block>';
            $(this).parent().append('<input type="hidden" name="data" value="'+dataToSend+'">');
            addIFrame($(this).parent());
        }
    });
})(jQuery);