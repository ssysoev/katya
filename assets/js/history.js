function viewSession() {
	var homePage = parseInt(localStorage.getItem("index"));
	var aboutPage = parseInt(localStorage.getItem("aboutme"));
	var hobbyPage = parseInt(localStorage.getItem("interests"));

	var studyPage = parseInt(localStorage.getItem("studying"));
	var testPage = parseInt(localStorage.getItem("test"));
	var galleryPage = parseInt(localStorage.getItem("photos"));

	var contactPage = parseInt(localStorage.getItem("contacts"));
	var historyPage = parseInt(localStorage.getItem("history"));

	document.getElementById("mainPage").innerHTML = returnResult(homePage);
	document.getElementById("aboutPage").innerHTML = returnResult(aboutPage);
	document.getElementById("hobbyPage").innerHTML = returnResult(hobbyPage);
	document.getElementById("studyPage").innerHTML = returnResult(studyPage);
	document.getElementById("testPage").innerHTML = returnResult(testPage);
	document.getElementById("galleryPage").innerHTML = returnResult(galleryPage);
	document.getElementById("contactPage").innerHTML = returnResult(contactPage);
	document.getElementById("historyPage").innerHTML = returnResult(historyPage);
}

function viewCookies() {
	var keyHome = "index";
	var keyAbout = "aboutme";
	var keyHobby = "interests";
	var keyStudy = "studying";
	var keyTest = "test";
	var keyGallery = "photos";
	var keyContact = "contacts";
	var keyHistory = "history";

	var valueHome = document.cookie.match(new RegExp(
		"(?:^|; )" + keyHome.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueAbout = document.cookie.match(new RegExp(
		"(?:^|; )" + keyAbout.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueHobby = document.cookie.match(new RegExp(
		"(?:^|; )" + keyHobby.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueStudy = document.cookie.match(new RegExp(
		"(?:^|; )" + keyStudy.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueTest = document.cookie.match(new RegExp(
		"(?:^|; )" + keyTest.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueGallery = document.cookie.match(new RegExp(
		"(?:^|; )" + keyGallery.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueContact = document.cookie.match(new RegExp(
		"(?:^|; )" + keyContact.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	var valueHistory = document.cookie.match(new RegExp(
		"(?:^|; )" + keyHistory.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));

	document.getElementById("mainPage2").innerHTML = valueHome ? decodeURIComponent(valueHome[1]) : "-";
	document.getElementById("aboutPage2").innerHTML = valueAbout ? decodeURIComponent(valueAbout[1]) : "-";
	document.getElementById("hobbyPage2").innerHTML = valueHobby ? decodeURIComponent(valueHobby[1]) : "-";
	document.getElementById("studyPage2").innerHTML = valueStudy ? decodeURIComponent(valueStudy[1]) : "-";
	document.getElementById("testPage2").innerHTML = valueTest ? decodeURIComponent(valueTest[1]) : "-";
	document.getElementById("galleryPage2").innerHTML = valueGallery ? decodeURIComponent(valueGallery[1]) : "-";
	document.getElementById("contactPage2").innerHTML = valueContact ? decodeURIComponent(valueContact[1]) : "-";
	document.getElementById("historyPage2").innerHTML = valueHistory ? decodeURIComponent(valueHistory[1]) : "-";
}

function returnResult(result) {
	if (isNaN(result)) {
		return " - "
	} else return result;
}