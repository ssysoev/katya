var FIO = false;
var DATE = false;
var EMAIL = false;
var MOBILE = false;
var MESSAGE = false;
var SEX = false;

function dynamicCheckFIO() {
    var nameInput = document.getElementById("name");
    var nameError = document.getElementById("nameError");
                                
    nameInput.onblur = function() {
        if (nameInput.value.length != 0 ) {
            massFIO = nameInput.value.split(' ');
            if (massFIO.length == "3" && massFIO[2] != undefined
                && massFIO[2].length != 0 && massFIO[3] == undefined) {
                nameInput.className ="correct";
                FIO = true;
                return;
            }
        } 
        FIO = false;
        nameInput.className = "error"; 
        nameError.innerHTML = '<h5>Введите имя, фамилию и отчество!</h5>'
    };

    nameInput.onfocus = function() {
        if (nameInput.className == 'error') { 
            nameInput.className = "";
            nameError.innerHTML = "";
        }
    };
 }

function dynamicCheckDate() {
    var dateOfBirth = document.getElementById("datebirth");
    var radioMen = document.getElementById("male");
    var radioWomen = document.getElementById("female");
    var radioOther = document.getElementById("other");
    
    dateOfBirth.onblur = function() {
        if(dateOfBirth.value.length != 0 ){
            dateOfBirth.className = "correct";
            DATE = true;
            return ; 
        }
        DATE = false;
        dateOfBirth.className = "error"; 
        dateError.innerHTML = '<h5>Введите дату рождения!</h5>'
    };

    dateOfBirth.onfocus = function() {
        if (dateOfBirth.className == 'error') { 
            dateOfBirth.className = "";
            dateError.innerHTML = "";
        }
        //radio check
        if(radioMen.checked || radioWomen.checked || radioOther.checked ){
            radioWomen.className = "";
            radioMen.className = "";
            radioOther.className = "";
            sexError.innerHTML = "";
            SEX = true;
            return; 
        }
        SEX = false;
        radioWomen.className = "error";
        radioMen.className = "error";
        radioOther.className = "error";
        sexError.innerHTML = '<h5>Выберите пол!</h5>'
    };
}

function dynamicCheckEMail(){
    var emailInput = document.getElementById("email");
    
    emailInput.onblur = function() {
        if(isNaN(emailInput.value)){
            emailInput.className = "correct";
            EMAIL = true;
            return ; 
        }
        EMAIL = false;
        emailInput.className = "error"; 
        emailError.innerHTML = '<h5>Введите свой e-mail!</h5>'
    };
    emailInput.onfocus = function() {
        if (emailInput.className == 'error') { 
            emailInput.className = "";
            emailError.innerHTML = "";
        }
    };
}

function dynamicCheckMobile() {
    var telInput = document.getElementById("phone");
    var positionOfSpace = telInput.value.match(/ /i );
    var telWithoutPlus = telInput.value.slice(1);
    
    telInput.onblur = function() {
        if (telInput.value.length >= 9 && telInput.value.length <= 12) {
            if (telInput.value[0] == "+" && (telInput.value.match(/\d/) == 7) || telInput.value.match(/\d/) == 3) { 
                if (telWithoutPlus.match(/\D/) == null) {
                    telInput.className = "correct";
                    MOBILE = true;
                    return;
                }
            }
        }  
        MOBILE = false;
        telInput.className = "error"; 
        mobileError.innerHTML = '<h5>Введите телефон без пробелов, от 9 до 11 цифр (+7/+3) </h5>'
    };
    telInput.onfocus = function() {
        if (telInput.className == 'error') { 
            telInput.className = "";
            mobileError.innerHTML = "";
        }
    };
}

function dynamicCheckMessage(){
    var messageText = document.getElementById("message");

    messageText.onblur = function() {
        if((messageText.value.length != 0)){
            messageText.className = "correct";
            MESSAGE = true
            return; 
        }
        MESSAGE = false;
        messageText.className = "error"; 
        messageError.innerHTML = '<h5>Напишите своё сообщение!</h5>'
    };
    messageText.onfocus = function() {
        if (messageText.className == 'error') { 
            messageText.className = "";
            messageError.innerHTML = "";
        }
    };
}

function dynamicCheckContacts() {
    var send = document.getElementById("send");
    console.log(send);

    send.onmouseover = function() {
    console.log(send);
        
        if (FIO && DATE && EMAIL && MOBILE && MESSAGE && SEX) {
            send.disabled = false;
            console.log(true);
            return true;
        } else {
            send.disabled = true;
            console.log(false);
        }
    }
}

function clearInput(){
    FIO = DATE = EMAIL = MOBILE = MESSAGE = SEX = false;
    alert("Ок!");
}   

function getDays(){
    document.getElementById('calendar').innerHTML = '';
    var month = document.getElementById("selectMonth").value;
    var year = document.getElementById("selectYear").value;
    var daysPerMonth = 32 - new Date(year, month, 32).getDate();

    for (var i = 1; i <= daysPerMonth; i++) {
        if(i == new Date().getDate() && month == new Date().getMonth() && year-1900 == new Date().getYear()) {
            document.getElementById('calendar').innerHTML += "<div class=\"today\">" + i + "</div>";
        } else {
            document.getElementById('calendar').innerHTML += "<div class=\"day\" id=\"d\" >" + i + "</div>";
        }
    }  
}

(function() {
    getDays();
    document.getElementById('selectMonth').addEventListener("change", getDays, false);
    document.getElementById('selectYear').addEventListener("change", getDays, false);
})();

$(document).ready(function() {
    $('#name').mouseover(function(){
        $('#modalFIO').css('display', 'block').animate({opacity: 1});
    });
    $('#male').mouseover(function(){
        $('#modalSex').css('display', 'block').animate({opacity: 1});
    });
    $('#datebirth').mouseover(function(){
        $('#modalDate').css('display', 'block').animate({opacity: 1});
    });
    $('#email').mouseover(function(){
        $('#modalEmail').css('display', 'block').animate({opacity: 1});
    });
    $('#phone').mouseover(function(){
        $('#modalNum').css('display', 'block').animate({opacity: 1});
    });
    $('#message').mouseover(function(){
        $('#modalMessage').css('display', 'block').animate({opacity: 1});
    });

    $('#name').mouseout( function(){
        setTimeout('close(\'#modalFIO\')', 1000);
    });
    $('#male').mouseout( function(){
        setTimeout('close(\'#modalSex\')', 1000);
    });
    $('#datebirth').mouseout( function(){
        setTimeout('close(\'#modalDate\')', 1000);
    });
    $('#email').mouseout( function(){
        setTimeout('close(\'#modalEmail\')', 1000);
    });
    $('#phone').mouseout( function(){
        setTimeout('close(\'#modalNum\')', 1000);
    });
    $('#message').mouseout( function(){
        setTimeout('close(\'#modalMessage\')', 1000);
    });
});

function close(id) {
    $(id).animate({opacity: 0},
        function(){
            $(this).css('display', 'none');
        });
}