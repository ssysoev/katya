/**
 * Created by serge on 10/9/16.
 */
function checkLogin() {
    var xhr = new XMLHttpRequest();
    var loginStatus = document.getElementById('login-status');
    var login = document.getElementById('login').value;
    if(login != '') {
        xhr.open('GET', '/?route=registration/checkLogin&login='+login);
        xhr.send();
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;

            if(xhr.responseText == 'available') {
                loginStatus.className = 'login-available';
                loginStatus.innerHTML = 'Логин свободен';
            } else {
                loginStatus.className = 'login-not-available';
                loginStatus.innerHTML = 'Логин занят';
            }
        }
        loginStatus.innerHTML = 'Проверка логина';
    } else {
        loginStatus.className = 'login-not-available';
        loginStatus.innerHTML = 'Введите логин!';
    }
}

function savePost(data, id) {
    var xhr = new XMLHttpRequest();

    xhr.open('POST', '/?route=editblog/updatePost');
    xhr.send(data);
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        $('div[data-post-id='+id+']').html(xhr.responseText);
        addListenerForOpenModalButton();
        addListenerForSaveButton();
    }
}

function addListenerForSaveButton() {
    $('button[class=save]').on('click', function() {
        var id = $(this).attr('data-post-id');
        var data = '<post>' +
            '<id>'+id+'</id>' +
            '<title>'+$('input[data-post-id='+id+']').val()+'</title>' +
            '<text>'+$('textarea[data-post-id='+id+']').val()+'</text>'+
            '</post>';
        savePost(data, id);
    });
}

(function ($) {
    addListenerForSaveButton();
})(jQuery);