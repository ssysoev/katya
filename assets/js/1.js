/*function loadImages(){
    var photoPath = ['css/f1.jpg', 'css/f2.jpg', 'css/f3.jpg', 'css/f4.jpg',
        'css/f5.jpg', 'css/f6.jpg', 'css/f7.jpg', 'css/f8.jpg',
        'css/f9.jpg', 'css/f10.jpg', 'css/f11.jpg', 'css/f12.jpg',
        'css/f13.jpg', 'css/f14.jpg', 'css/f15.jpg'];

    var photoTitles = ['Это я', 'Это я','Это я','Это я',
        'Это я','Это я','Это я','Это я',
        'Это я','Это я','Это я','Это я',
        'Это я','Это я','Это я'];


    var gallery = document.getElementById('gallery');

    var modal = document.getElementById('myModal');
    var img = document.getElementById('myImg');
	var modalImg = document.getElementById("img01");
	var captionText = document.getElementById("caption");

   for(var key in photoPath) {
    var myImage = document.createElement("img"); 
    myImage.src = photoPath[key];
    myImage.alt = photoTitles[key];
    myImage.id = 'myImg';
    gallery.appendChild(myImage);
    myImage.addEventListener('click', function() {
	    modal.style.display = "block";
	    modalImg.src = this.src;
	    modalImg.alt = this.alt;
	    captionText.innerHTML = this.alt;
    });
   }
*/
    // for (var i = 0; i < photoPath.length; i++)
    // {
    //     document.images[i].src = photoPath[i];
    //     document.images[i].title = photoTitles[i];
    // }


function checkTest() {
    var radio1 = document.getElementById("r1");
    var radio2 = document.getElementById("r2");
    var radio3 = document.getElementById("r3");
    var radio4 = document.getElementById("r4");
    var selectAnswer = document.getElementById("q2");
    var answerInput = document.getElementById("t1");

    if (!radio1.checked && !radio2.checked && !radio3.checked && !radio4.checked ) {
        alert("Выберите ответ на вопрос 1!");
        radio1.focus();
        return false;

    } else if (selectAnswer.value =="-1") {
        alert("Выберите ответ на вопрос 2!");
        selectAnswer.focus();
        return false;

    } else if (checkInt() == false) {
        answerInput.focus();
        return false;
    }
}

function checkInt() {
    var answerInput = document.getElementById("t1");

    if(answerInput.value.length != 0) {

        if (answerInput.value.match(/\D/) == null) {

            if(isInteger(answerInput.value)) {
                return true;
            }
        }
    }

    alert("Ответ на вопрос 3 некорректен!");
    return false;
}

(function() {
    document.getElementById('name').addEventListener('focusout', checkFIO);
})();

function isInteger(number) {
    return (number ^ 0) === +number;
}