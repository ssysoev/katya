/**
 * Created by serge on 10/9/16.
 */
function addListenerForOpenModalButton() {
    $('button[class*=open-modal]').on('click', function() {
        var id = $(this).attr('data-modal-id');
        $('div[data-modal-id='+id+']').show();
    });
    $('span[class=close]').on('click', function () {
        $(this).parents('.modal').hide();
    });
}
(function($) {
    addListenerForOpenModalButton();
})(jQuery);