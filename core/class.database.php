<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 30.05.16
 * Time: 19:57
 */
class Database
{
    private $registry;
    private $db;

    function __construct($registry) {
        $this->registry = $registry;
    }

    function create() {
        try {
            $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS,
                [PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION]);
        }catch(PDOException $e) {
            echo "Подключение к БД не удалось".$e->getMessage()."<br />";
        }
    }

    public function save($table, $key_value_array) {
        $sql = 'INSERT INTO ' . $table . ' ';
        $columns = '(';
        $values = '(';
        foreach ($key_value_array as $k => $v) {
            $columns .= '`' . $k . '`, ';
            if ($k == 'id' && $v == 'NULL') {
                $values .= " NULL, ";
            } else {
                $values .= "'" . $v . "', ";
            }
        }
        $columns = rtrim($columns, ', ') . ')';
        $values = rtrim($values, ', ') . ')';
        $sql .= $columns . ' VALUES ' . $values;
        $q = $this->db->prepare($sql);
        $q->execute();
    }

    public function read($table, $id) {
        $sql = "SELECT * FROM $table WHERE `login` = :id";
        $q = $this->db->prepare($sql);
        $q->bindValue(':id', $id, PDO::PARAM_STR);
        $q->execute();
        return $q->fetch(PDO::FETCH_ASSOC);
    }

    private function debugInfo($var) {
        echo "<pre>", var_dump($var), "</pre>";
    }

    public function readAll($table) {
        $sql = "SELECT * FROM $table";
        $q = $this->db->prepare($sql);
        $q->execute();
        //$this->debugInfo($q->fetchAll(PDO::FETCH_ASSOC));
        return $q->fetchAll(PDO::FETCH_ASSOC);
    }

    public function readOne($table, $field, $value) {
        $sql = "SELECT * FROM $table WHERE `$field` = :field";
        $q = $this->db->prepare($sql);
        $q->bindValue(':field', $value, PDO::PARAM_STR);
        $q->execute();
        return $q->fetch(PDO::FETCH_ASSOC);
    }


    public function update($table, $fields, $id) {
        $sql = 'UPDATE ' . $table . ' SET ';
        foreach ( $fields as $key => $field ) {
            $sql .= "`$key` = '$field', ";
        }
        $sql = rtrim($sql, ', ') . ' ';
        $sql .= 'WHERE id = ' . $id;
        $q = $this->db->prepare($sql);
        $q->execute();
    }

    public  function delete() {

        return 2;
    }

    function findAll() {

        return 1;
    }
}