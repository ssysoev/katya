<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:37
 */

Class Router {

    private $registry;
    private $path;
    private $args = array();

    function __construct($registry) {
        $this->registry = $registry;
    }

    function setPath($path) {
        $path .= DS;
        if (is_dir($path) == false) {
            throw new Exception ('Invalid controller path: `' . $path . '`');
        }
        $this->path = $path;
    }

    private function getController(&$file, &$controller, &$action, &$args) {
        $route = (empty($_GET['route'])) ? '' : $_GET['route'];
        if (empty($route)) {
            $route = 'index';
        }
        $parts = explode('/', $route);
        $cmd_path = $this->path;
        foreach ($parts as $part) {
            $fullPath = $cmd_path . $part;
            if (is_dir($fullPath)) {
                $cmd_path .= $part . DS;
                array_shift($parts);
                continue;
            }

            if (is_file($fullPath . '.php')) {
                $controller = $part;
                array_shift($parts);
                break;
            }
        }

        if (empty($controller)) { $controller = 'index'; };
        // Получаем действие
        $action = array_shift($parts);
        if (empty($action)) { $action = 'index'; }
        $file = $cmd_path . $controller . '.php';
        $args = $parts;
    }

    function delegate() {
        // Анализируем путь
        $this->getController($file, $controller, $action, $args);

        // Файл доступен?
        if (is_readable($file) == false) {
            die ('404 Not Found. There are no '. $controller);
        }
        // Подключаем файл
        include ($file);

        // Создаём экземпляр контроллера
        $class = 'Controller_' . $controller;
        $controller = new $class($this->registry);
        // Действие доступно?
        if (is_callable(array($controller, $action)) == false) {
            die ('404 Not Found');
        }
        // Выполняем действие
        $controller->$action();
    }
}