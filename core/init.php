<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:24
 */

function __autoload($class_name) {

    $filename = strtolower($class_name) . '.php';
    $file = SITE_PATH . 'core' . DS . 'class.' . $filename;

    if (file_exists($file) == false) {
        return false;
    }

    include ($file);
}

$registry = new Registry;