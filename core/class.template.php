<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 22:29
 */

Class Template {

    private $registry;
    private $vars = array();

    function __construct($registry) {
        $this->registry = $registry;
    }

    function set($varName, $value, $overwrite=false) {
        if (isset($this->vars[$varName]) == true AND $overwrite == false) {
            trigger_error ('Unable to set var `' . $varName . '`. Already set, and overwrite not allowed.', E_USER_NOTICE);
            return false;
        }

        $this->vars[$varName] = $value;
        return true;
    }

    function remove($varName) {
        unset($this->vars[$varName]);
        return true;
    }

    function show($name) {
        $path = SITE_PATH . 'views' . DS . $name . '.php';
        if (file_exists($path) == false) {
            trigger_error ('Template `' . $name . '` does not exist.', E_USER_NOTICE);
            return false;
        }
        // Load variables
        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }
        include ($path);
    }
    function showModel($name,$vars/*$vars = array()*/) {
        $path = SITE_PATH . 'views' . DS . $name . '.php';
        if (file_exists($path) == false) {
            trigger_error ('Template `' . $name . '` does not exist.', E_USER_NOTICE);
            return false;
        }
        // Load variables
        /*   foreach ($this->vars as $key => $value) {
               $$key = $value;
           }*/

        $table = "  <tr>
                    <td>id </td>
                    <td>id теста</td>
                    <td>Ответы</td>
                    <td>ФИО</td>
                    <td>Дата</td>
                </tr>";

        foreach ( $vars as $var ) {
            $table.= "<tr>";
            foreach ( $var as $key => $value ) {
                $table.= "<td>".$value."</td>";
            }
            $table.= "</tr>";
        }

        $content = file_get_contents($path);
        $content = str_replace('{*results*}',$table, $content);
        echo $content;
    }

    function showBlog($name, $vars)
    {
        $path = SITE_PATH . 'views' . DS . $name . '.php';
        if (file_exists($path) == false) {
            trigger_error('Template `' . $name . '` does not exist.', E_USER_NOTICE);
            return false;
        }

        $result="";
        foreach ($vars['posts'] as $var) {
            $result .= '<div class="post">';
            $result .= "<h3>" . $var['theme'] . "</h3>" . $var['text'];
            foreach ( $vars['comments'] as $comment ) {
                if ( $comment['post_id'] == $var['id'] ) {
                    $result .= '
                            <div class="comment">
                                <span>Комментарий написан </span>
                                <span class="date">'. $comment['date'] .'</span>
                                <span class="author">Автор: '. $comment['author'] .'</span>
                                <p class="comment-text">'. $comment['comment'] .'</p>
                            </div>
                       ';
                }
            }
            if(isset($_SESSION['vlogin'])) {
                $result .= '<br><button class="toggle-comment-form" data-post-id="'.$var['id'].'">Оставить комментарий</button>';
                $result .= '<form action="/?route=blog/addComment" method="post" target="asyncFrame" class="comment-form">
                    <textarea id="new-comment-text"></textarea><br>
                    <button class="add-comment" data-post-id="'.$var['id'].'">Добавить комментарий</button>
                </form>';
            }
            $result .= '</div>';
        }

        $content = file_get_contents($path);
        $content = str_replace('{*results*}', $result, $content);
        echo $content;
    }

    public function showPostsAdmin($name, $posts) {
        $path = SITE_PATH . 'views' . DS . $name . '.php';
        if (file_exists($path) == false) {
            trigger_error('Template `' . $name . '` does not exist.', E_USER_NOTICE);
            return false;
        }

        $result="";
        foreach ($posts as $post) {
            $result .= '<div class="post" data-post-id="'.$post['id'].'">';
            $result .= "<h3>" . $post['theme'] . "</h3>" . $post['text'];
            $result .= '<br><button class="open-modal edit-post" data-modal-id="'.$post['id'].'">Редактировать</button>';
            $result .= '<div data-modal-id="'.$post['id'].'" class="modal">
                          <div class="modal-content">
                            <div class="modal-body">
                              <span class="close">×</span>
                              <input type="text" data-post-id="'.$post['id'].'" value="'.$post['theme'].'"><br>
                              <textarea cols="50" rows="10" data-post-id="'.$post['id'].'">'.$post['text'].'</textarea><br>
                              <button class="save" data-post-id="'.$post['id'].'">Сохранить</button>
                            </div>
                          </div>
                        </div>';
            $result .= '</div>';
        }

        $content = file_get_contents($path);
        $content = str_replace('{*posts*}', $result, $content);
        echo $content;
    }
}