<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:52
 */

Abstract Class Controller_Base {

    protected $registry;

    function __construct($registry) {
        $this->registry = $registry;
    }

    abstract function index();
}
