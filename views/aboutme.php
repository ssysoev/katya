<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<script src = "/assets/js/scripts.js" type="text/javascript"></script>
	<link rel=stylesheet type="text/css" href="/assets/css/style.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<title>Лабораторная работа №7</title>
</head>

<body>
<div id="nav">
	<ul>
		<li><a href="index.php">Главная</a></li>
		<li><a href="?route=aboutme" class="now">Обо мне</a></li>
		<li id="hobbyMenu" ><a>Интересы</a>
			<ul id="subHobbyMenu">
				<li><a href="?route=interests#hobby">Мои хобби</a></li>
				<li><a href="?route=interests#books">Любимые книги</a></li>
				<li><a href="?route=interests#music">Любимая музыка</a></li>
				<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
				<li><a href="?route=interests#films">Любимые фильмы</a></li>
				<li><a href="?route=interests#games">Любимые игры</a></li>
				<li><a href="?route=interests#rso">Общественная деятельность</a></li>
			</ul>
		</li>
		<li><a href="?route=studying">Учёба</a></li>
		<li><a href="?route=photos">Фотоальбом</a></li>
		<li><a href="?route=contacts">Контакты</a></li>
		<li><a href="?route=history">История</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
<div id="wrapper">
	<div id="container">
		<fieldset>
			<legend>Начало</legend>
			<p>Я родилась 5 января 1996  года в городе-герое Севастополе.
			<br>Была первым и самым любимым ребёнком в семье, таким являюсь и до сих пор :)</p>
		</fieldset>
		<fieldset>
			<legend>Учёба в ранние годы</legend>
			<p>В три года меня отдали в д/с №130, где меня ничему не учили, кроме произношения буквы Р, поэтому я занималась дома.
				<br>Читать и писать меня научили бабушка с дедушкой, они же привили тягу к новым знаниям.</p>
		</fieldset>
		<fieldset>
			<legend>Учёба в школе</legend>
			<p>В 2002 году я пошла в первый класс СОШ №54 города Севастополя. На тот момент ей ещё не было присвоено имя Юрия Алексеевича Гагарина, да и неважно.<br>
			Училась я всегда хорошо. С 3 класса участвовала в олимпиадах, где достаточно часто занимала призовые места. <br>
			Особенно хорошо мне давались языки, в частности украинский. Жаль, что оно мне не пригодилось :)</p>
		</fieldset>
		<fieldset>
			<legend>После школы</legend>
			<p>Закончив учёбу в этом замечательном учебном заведении, мне предоставился огромный выбор будущей профессии и ВУЗа, где я осваивала бы её.
			<br>Всей школой меня отправляли в ХАИ на "Компьютерные науки", но в силу обстоятельств я осталась в Севастополе и поступила в СевНТУ,
			о чём теперь нисколько не жалею.</p>
		</fieldset>
		<fieldset>
			<legend>А сейчас..</legend>
			<p>Сейчас я учусь на третьем курсе этой прекрасной специальности.<br>
			Не все предметы даются мне легко, но я стараюсь, честно.</p>
		</fieldset>
	</div>
</div>
<script>
	hobbyMenu();
	visitPage("aboutme");
	setCookie("aboutme");
</script>
</body>
</html>