<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="/assets/css/history.css">
        <script src = "/assets/js/scripts.js" type="text/javascript"></script>
        <script src = "/assets/js/history.js" type="text/javascript"></script>

        <link rel=stylesheet type="text/css" href="/assets/css/style.css">
        <link rel=stylesheet type="text/css" href="/assets/css/menu.css">
        <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    	<title>Лабораторная работа №7</title>
    </head>

    <body>
    <div id="nav">
        <ul>
            <li><a href="index.php">Главная</a></li>
            <li><a href="?route=aboutme">Обо мне</a></li>
            <li id="hobbyMenu" ><a>Интересы</a>
                <ul id="subHobbyMenu">
                    <li><a href="?route=interests#hobby">Мои хобби</a></li>
                    <li><a href="?route=interests#books">Любимые книги</a></li>
                    <li><a href="?route=interests#music">Любимая музыка</a></li>
                    <li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
                    <li><a href="?route=interests#films">Любимые фильмы</a></li>
                    <li><a href="?route=interests#games">Любимые игры</a></li>
                    <li><a href="?route=interests#rso">Общественная деятельность</a></li>
                </ul>
            </li>
            <li><a href="?route=studying">Учёба</a></li>
            <li><a href="?route=photos">Фотоальбом</a></li>
            <li><a href="?route=contacts">Контакты</a></li>
            <li><a href="?route=history" class="now">История</a></li>
        </ul>
                <p id="time"></p>
                <script>
                    setInterval(showDateAndTime, 100);
                </script>            
 </div>

        <div class = "wrapper">
            
            <div class="container">
                    <table class = "tables" align="center" border = 1 bordercolor = #90caf9>
                        <tr>
                            <td colspan="2">История текущего сеанса<br></td>
                        </tr>
                        <tr>
                            <td>Имя страницы<br></td>
                            <td>Посещений<br></td>
                        </tr>
                        <tr>
                            <td>Главная</td>
                            <td><div id="mainPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Обо мне<br></td>
                            <td><div id="aboutPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Хобби</td>
                            <td><div id="hobbyPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Учёба</td>
                            <td><div id="studyPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Тест</td>
                            <td><div id="testPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Галерея</td>
                            <td><div id="galleryPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Контакты</td>
                            <td><div id="contactPage" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>История</td>
                            <td><div id="historyPage" align="center"></div></td>
                        </tr>
                    </table>

                    <table class = "tables" align="center" border = 1 bordercolor = #90caf9>
                        <tr>
                            <td colspan="2">История посещений<br></td>
                        </tr>
                        <tr>
                            <td>Имя страницы<br></td>
                            <td>Посещений<br></td>
                        </tr>
                        <tr>
                            <td>Главная</td>
                            <td><div id="mainPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Обо мне<br></td>
                            <td><div id="aboutPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Хобби</td>
                            <td><div id="hobbyPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Учёба</td>
                            <td><div id="studyPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Тест</td>
                            <td><div id="testPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Галерея</td>
                            <td><div id="galleryPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>Контакты</td>
                            <td><div id="contactPage2" align="center"></div></td>
                        </tr>
                        <tr>
                            <td>История</td>
                            <td><div id="historyPage2" align="center"></div></td>
                        </tr>
                    </table>
            </div>

        </div>



        <script>
            hobbyMenu();
            setCookie("history");
            viewCookies();
            visitPage("history");
            viewSession();

            window.onbeforeunload = localStorage.clear();
        </script>
    </body>
</html>  
