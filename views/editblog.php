<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Лабораторная работа №7</title>
    <link rel=stylesheet type="text/css" href="/assets/css/style_main.css">
    <link rel=stylesheet type="text/css" href="/assets/css/menu.css">
    <script src="/assets/js/scripts.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
    <ul>
        <li><a href="?route=editblog">Edit blog</a></li>
        <li><a href="?route=loadguestbook">Load guest</a></li>
        <li><a href="?route=admin">Admin</a></li>
        <li><a href="?route=admin/exitSession">Exit</a></li>
    </ul>
    <h4 id="time"></h4>
    <script>
        setInterval(showDateAndTime, 100);
    </script>
</div>
<div id="wrapper">
    <div id = "container">
        <h1 align="center">Редактор блога</h1>
        <h3 align="center">Добавьте запись</h3>

        <form action="?route=editblog/upload" method="post">

            <h3>Тема записи:</h3>
            <p><input id="them" name="theme" class="inputContact" type="text" size="40" placeholder="Name Lastname Surname"><br></p>

            <h3>Выберите файл:</h3>
            <p><input id="name" name="file" class="inputContact" type="file"><br></p>

            <h3>Текст записи:</h3>
            <p><textarea  id="messageText" name="message" class="inputContact" cols=41 rows=6 placeholder="Text of message"></textarea><br></p>

            <p><input type="submit" value="Отправить"></p>
        </form>
        <div class="posts">
            {*posts*}
        </div>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/modal.js"></script>
<script src="/assets/js/ajax.js"></script>
<script>
    hobbyMenu();
</script>
</body>
</html>
