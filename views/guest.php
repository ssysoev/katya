<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Лабораторная работа №7</title>
    <script src = "/assets/js/scripts.js" type="text/javascript"></script>
    <link rel=stylesheet type="text/css" href="/assets/css/style.css">
    <link rel=stylesheet type="text/css" href="/assets/css/menu.css">
    <link rel=stylesheet type="text/css" href="/assets/css/contact.css">
    <script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
    <ul>
        <li><a href="index.php">Главная</a></li>
        <li><a href="?route=aboutme">Обо мне</a></li>
        <li id="hobbyMenu" ><a>Интересы</a>
            <ul id="subHobbyMenu">
                <li><a href="?route=interests#hobby">Мои хобби</a></li>
                <li><a href="?route=interests#books">Любимые книги</a></li>
                <li><a href="?route=interests#music">Любимая музыка</a></li>
                <li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
                <li><a href="?route=interests#films">Любимые фильмы</a></li>
                <li><a href="?route=interests#games">Любимые игры</a></li>
                <li><a href="?route=interests#rso">Общественная деятельность</a></li>
            </ul>
        </li>
        <li><a href="?route=studying">Учёба</a></li>
        <li><a href="?route=photos">Фотоальбом</a></li>
        <li><a href="?route=contacts" class="now">Контакты</a></li>
        <li><a href="?route=history">История</a></li>
    </ul>
    <h5 id="time"></h5>
    <script>
        setInterval(showDateAndTime, 100);
    </script>
</div>

<div id="wrapper">
    <div id="mailblock">

        <h1 align="center">Гостевая книга</h1>
        <table class = "tables" align="center" border = 1 bordercolor = #90caf9>
            <tr>
                <td>ФИО</td>
                <td>E-mail</td>
                <td>Отзыв</td>
                <td>Дата</td>
            </tr>
            <?php
            $fp = fopen("data/guestbook.inc", "r"); // Открываем файл в режиме чтения
            $rows = file(dirname(__FILE__) . DIRECTORY_SEPARATOR . $fileName);
            if ($fp) {
                while(!feof($fp)) {
                    $string = explode(';', fgets($fp));
                    echo "<tr>";
                    foreach ($string as $value) {
                        echo "<td>", $value, "</td>";
                    }
                    echo "</tr>";
                }
            }
            ?>
        </table>


        <form action="/?route=guest/upload" method="post" enctype="multipart/form-data" >
            <p class="note"><span class="req">*</span> Поля со звездочкой обязательны для заполнения</p>
            <div class="row">
                <label for="name">Ваше ФИО <span class="req">*</span></label>
                <input type="text" name="fio" id="name" class="txt" tabindex="1" placeholder="Ваши Фамилия Имя Отчество..." onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();" required>
                <div id="nameError" class="warning" ></div>
            </div>
            <div id="modalFIO">
                <h5>Введите свои ФИО!</h5>
            </div>

            <div class="row">
                <label for="email">E-mail <span class="req">*</span></label>
                <input type="text" name="email" id="email" class="txt" tabindex="2" placeholder=" address@mail.ru" onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();" required>
                <div id="emailError" class="warning" ></div>
            </div>

            <div id="modalEmail">
                <h5>Введите свой e-mail адрес!</h5>
            </div>


            <div class="row">
                <label for="message">Сообщение <span class="req">*</span></label>
                <textarea name="message" id="message" class="txtarea" tabindex="1" rows=10 cols =40 onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();"required></textarea>
            </div>
            <div id="messageError" class="warning" ></div>
            <div id="modalMessage">
                <h5>Введите Ваше сообщение!</h5>
            </div>

            <div class="center">
                <input class="check" type="submit" value="Отправить cообщение">
                <input class="check" type="reset" value="Сброс" onClick="clearInput()">
            </div>
        </form>
    </div>
</div>
<script src = "/assets/js/contact.js" type="text/javascript"></script>

<script>
    hobbyMenu();

    dynamicCheckFIO();

    dynamicCheckEMail();

    dynamicCheckMessage();

</script>
</body>
</html>