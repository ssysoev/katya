<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Лабораторная работа №7</title>
	<script src = "/assets/js/scripts.js" type="text/javascript"></script>
    <link rel=stylesheet type="text/css" href="/assets/css/style.css">
    <link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<link rel=stylesheet type="text/css" href="/assets/css/contact.css">
	<script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
	<ul>
		<li><a href="index.php">Главная</a></li>
		<li><a href="?route=aboutme">Обо мне</a></li>
		<li id="hobbyMenu" ><a>Интересы</a>
			<ul id="subHobbyMenu">
				<li><a href="?route=interests#hobby">Мои хобби</a></li>
				<li><a href="?route=interests#books">Любимые книги</a></li>
				<li><a href="?route=interests#music">Любимая музыка</a></li>
				<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
				<li><a href="?route=interests#films">Любимые фильмы</a></li>
				<li><a href="?route=interests#games">Любимые игры</a></li>
				<li><a href="?route=interests#rso">Общественная деятельность</a></li>
			</ul>
		</li>
		<li><a href="?route=studying">Учёба</a></li>
		<li><a href="?route=photos">Фотоальбом</a></li>
		<li><a href="?route=contacts" class="now">Контакты</a></li>
		<li><a href="?route=history">История</a></li>
	</ul>
	<h5 id="time"></h5>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>

<div id="wrapper">
	<div id="mailblock">
		<h1>Напишите мне!</h1>
		<form id="contactform" name="contact" method="get|post" action="MAILTO:katya.galshina@gmail.com" >
			<p class="note"><span class="req">*</span> Поля со звездочкой обязательны для заполнения</p>
			<div class="row">
				<label for="name">Ваше ФИО <span class="req">*</span></label>
				<input type="text" name="name" id="name" class="txt" tabindex="1" placeholder="Ваши Фамилия Имя Отчество..." onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();" required>
				<div id="nameError" class="warning" ></div>
			</div>
			<div id="modalFIO">
				<h5>Введите свои ФИО!</h5>
			</div>

			<div class="row">
				<label for="email">E-mail <span class="req">*</span></label>
				<input type="text" name="email" id="email" class="txt" tabindex="2" placeholder=" address@mail.ru" onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();" required>
				<div id="emailError" class="warning" ></div>
			</div>
			<div id="modalEmail">
				<h5>Введите свой e-mail адрес!</h5>
			</div>

			<div class="row">
				<label for="email">Ваш пол <span class="req">*</span></label><br>
				<input type="radio" name="gender" id="male" value="male" onclick="dynamicCheckContacts();">
				<label for="male"> мужской </label><br>
				<input type="radio" name="gender" id="female" value="female" onclick="dynamicCheckContacts();">
				<label for="female"> женский</label><br>
				<input type="radio" name="gender" id="other" value="other" onclick="dynamicCheckContacts();">
				<label for="other"> ой, всё :с </label><br><br>
				<br>
			</div>
			<div id="sexError" class="warning" ></div>
			<div id="modalSex">
				<h5>Выберите Ваш пол!</h5>
			</div>

			<div class="row">
				<label for="datebirth">Дата рождения <span class="req">*</span></label>
				<input type="text" name="birth" id="datebirth" class="txt" tabindex="1" onclick="dynamicCheckContacts();" required>
			</div>
			<div id="dateError" class="warning" ></div>
			<div id="modalDate">
				<h5>Введите свою дату рождения!</h5>
			</div>

			<div id="calendarHeader">
				<p><select id="selectMonth">
					<option value="0">Январь</option>
					<option value="1">Февраль</option>
					<option value="2">Март</option>
					<option value="3">Апрель</option>
					<option value="4">Май</option>
					<option value="5">Июнь</option>
					<option value="6">Июль</option>
					<option value="7">Август</option>
					<option value="8">Сентябрь</option>
					<option value="9">Октябрь</option>
					<option value="10">Ноябрь</option>
					<option value="11">Декабрь</option>
				</select>

					<select id="selectYear">
						<option value="2016">2016</option>
						<option value="2015">2015</option>
						<option value="2014">2014</option>
						<option value="2013">2013</option>
						<option value="2012">2012</option>
						<option value="2011">2011</option>
						<option value="2010">2010</option>
						<option value="2009">2009</option>
						<option value="2008">2008</option>
						<option value="2007">2007</option>
						<option value="2006">2006</option>
						<option value="2005">2005</option>
					</select>
				</p>
			</div>

			<div id ="calendar"></div>

			<div class="row">
				<label for="phone">Номер телефона <span class="req">*</span></label>
				<input type="text" name="phone" id="phone" class="txt" tabindex="1" onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();" required>
			</div>
			<div id="mobileError" class="warning" ></div>
			<div id="modalNum">
				<h5>Введите свой номер телефона (+7/+3)!</h5>
			</div>

			<div class="row">
				<label for="message">Сообщение <span class="req">*</span></label>
				<textarea name="message" id="message" class="txtarea" tabindex="1" rows=10 cols =40 onkeyup="dynamicCheckContacts();" onkeypress="dynamicCheckContacts();" onchange="dynamicCheckContacts();"required></textarea>
			</div>
			<div id="messageError" class="warning" ></div>
			<div id="modalMessage">
				<h5>Введите Ваше сообщение!</h5>
			</div>

			<div class="center">
				<input class="check" type="submit" id="send" disabled="disabled" value="Отправить cообщение">
				<input class="check" type="reset" value="Сброс" onClick="clearInput()">
			</div>
		</form>
	</div>
</div>
<script src = "/assets/js/contact.js" type="text/javascript"></script>

<script>
    hobbyMenu();
	dynamicCheckContacts();
	dynamicCheckFIO();
	dynamicCheckDate();
	dynamicCheckEMail();
	dynamicCheckMobile();
	dynamicCheckMessage();

	visitPage("contacts");
	setCookie("contacts");
</script>
</body>
</html>