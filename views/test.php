<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Лабораторная работа №7</title>
	<link rel=stylesheet type="text/css" href="/assets/css/style.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<script src="/assets/js/scripts.js" type="text/javascript"></script>
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
	<ul>
		<li><a href="index.php">Главная</a></li>
		<li><a href="?route=aboutme" >Обо мне</a></li>
		<li id="hobbyMenu" ><a>Интересы</a>
			<ul id="subHobbyMenu">
				<li><a href="?route=interests#hobby">Мои хобби</a></li>
				<li><a href="?route=interests#books">Любимые книги</a></li>
				<li><a href="?route=interests#music">Любимая музыка</a></li>
				<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
				<li><a href="?route=interests#films">Любимые фильмы</a></li>
				<li><a href="?route=interests#games">Любимые игры</a></li>
				<li><a href="?route=interests#rso">Общественная деятельность</a></li>
			</ul>
		</li>
		<li><a href="?route=studying">Учёба</a></li>
		<li><a href="?route=photos">Фотоальбом</a></li>
		<li><a href="?route=contacts">Контакты</a></li>
		<li><a href="?route=history">История</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
<div id="wrapper">
	<div id = "container">
<form action="?route=test/send" method="post">
	<br>
	<p>ФИО:  </p>
	<input type="text" id="name"  class="check" name="fio" placeholder="Ваше ФИО..." required><br>
	<br>
	<br>
	<p>Вопрос 1: Что представляет собой универсальное множество?</p>
		<br>
		<input type="radio" id="r1" value="1" name="Q1"> имеет то свойство, при котором включает все подмножества для входного множества
		<br>
		<input type="radio" id="r2" value="2" name="Q1"> это эквивалент для сравнения
		<br>
		<input type="radio" id="r3" value="3" name="Q1"> это декартово произведение на множестве
		<br>
		<input type="radio" id="r4" value="4" name="Q1"> имеет такую особенность, когда все множества являются ее подмножествами
		<br>
	<br>

	<p>Вопрос 2: Сколько различных слов можно получить перестановками букв в слове abcde?</p>
	<select id="q2" class="check" name ="Q2">
	<option value = "5">5</option>
	<option value = "55">55</option>
	<option value = "120">120</option>
	<option value = "20">20</option>
	<option value = "70">70</option>
	</select><br>
	<br>

	<p>Вопрос 3: Сколько существует логических функций от двух переменных?</p>
<input type="text"  id="t1" class="check" name="Q3"><br>


<br><br>
<input class="check" type="submit" value="Send" onClick="return checkTest()">
<input class="check" type="reset" value="Reset">
</form>
	</div>
</div>
<script>
	hobbyMenu();
	visitPage("test");
	setCookie("test");
</script>
</body>
</html>
