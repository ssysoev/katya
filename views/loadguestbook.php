<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Лабораторная работа №7</title>
    <script src = "/assets/js/scripts.js" type="text/javascript"></script>
    <link rel=stylesheet type="text/css" href="/assets/css/style.css">
    <link rel=stylesheet type="text/css" href="/assets/css/menu.css">
    <link rel=stylesheet type="text/css" href="/assets/css/contact.css">
    <script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
    <ul>
        <li><a href="?route=editblog">Edit blog</a></li>
        <li><a href="?route=loadguestbook">Load guest</a></li>
        <li><a href="?route=admin">Admin</a></li>
        <li><a href="?route=admin/exitSession">Exit</a></li>
    </ul>
    <h5 id="time"></h5>
    <script>
        setInterval(showDateAndTime, 100);
    </script>
</div>

<div id="wrapper">
    <div id="mailblock">

        <h1 align="center">Гостевая книга</h1>

        <form action="?route=loadguestbook/upload" method="post" enctype="multipart/form-data">
            <h3>Выберите файл:</h3>
            <p><input id="name" name="file" class="inputContact" type="file"><br></p>

            <p><input type="submit" value="Отправить" onClick="">
            </p>
        </form>


    </div>
</div>
<script src = "/assets/js/contact.js" type="text/javascript"></script>

<script>
    hobbyMenu();
</script>
</body>
</html>