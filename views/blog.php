<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Лабораторная работа №7</title>
    <link rel=stylesheet type="text/css" href="/assets/css/style.css">
    <link rel=stylesheet type="text/css" href="/assets/css/menu.css">
    <script src="/assets/js/scripts.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
    <ul>
        <li><a href="index.php">Главная</a></li>
        <li><a href="?route=aboutme" >Обо мне</a></li>
        <li id="hobbyMenu" ><a>Интересы</a>
            <ul id="subHobbyMenu">
                <li><a href="?route=interests#hobby">Мои хобби</a></li>
                <li><a href="?route=interests#books">Любимые книги</a></li>
                <li><a href="?route=interests#music">Любимая музыка</a></li>
                <li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
                <li><a href="?route=interests#films">Любимые фильмы</a></li>
                <li><a href="?route=interests#games">Любимые игры</a></li>
                <li><a href="?route=interests#rso">Общественная деятельность</a></li>
            </ul>
        </li>
        <li><a href="?route=studying">Учёба</a></li>
        <li><a href="?route=photos">Фотоальбом</a></li>
        <li><a href="?route=contacts">Контакты</a></li>
        <li><a href="?route=history">История</a></li>
    </ul>
    <h4 id="time"></h4>
    <script>
        setInterval(showDateAndTime, 100);
    </script>
</div>
<div id="wrapper">
    <div id = "container">
        <h1 align="center">Мой блог</h1>
        {*results*}
        
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/comments.js"></script>
<script>
    hobbyMenu();
</script>
</body>
</html>
