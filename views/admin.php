<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<script src = "/assets/js/scripts.js" type="text/javascript"></script>
	<script src = "/assets/js/home.js" type="text/javascript"></script>
	<script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
	<link rel=stylesheet type="text/css" href="/assets/css/style_main.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<title>Лабораторная работа №7</title>
</head>

<body>
<div id="nav">
	<ul>
		<li><a href="?route=editblog">Edit blog</a></li>
		<li><a href="?route=loadguestbook">Load guest</a></li>
		<li><a href="?route=admin">Admin</a></li>
		<li><a href="?route=admin/exitSession">Exit</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
<div id="wrapper">
	<div id="info">
		<h1 align="center">Гальшина Екатерина Алексеевна </h1>
		<h2 align="center">Admin</h2>
		<h3 align="center">
			<a align="center" href="?route=editblog">Редактор блога блог!</a>
		</h3>
		<h3 align="center">
			<a align="center" href="?route=loadguestbook">Загрузить файла отзывов на сервер!</a>
		</h3>
	</div>
</div>

	<div id="footer">
		<h3>SevGU, 2016</h3>
	</div>
</div>
<script>
	hobbyMenu();
</script>
</body>
</html>