<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Лабораторная работа №7</title>
	<link rel=stylesheet type="text/css" href="/assets/css/style.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<link rel=stylesheet type="text/css" href="/assets/css/photos.css">
	<script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
	<ul>
		<li><a href="index.php">Главная</a></li>
		<li><a href="?route=aboutme">Обо мне</a></li>
		<li id="hobbyMenu" ><a>Интересы</a>
			<ul id="subHobbyMenu">
				<li><a href="?route=interests#hobby">Мои хобби</a></li>
				<li><a href="?route=interests#books">Любимые книги</a></li>
				<li><a href="?route=interests#music">Любимая музыка</a></li>
				<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
				<li><a href="?route=interests#films">Любимые фильмы</a></li>
				<li><a href="?route=interests#games">Любимые игры</a></li>
				<li><a href="?route=interests#rso">Общественная деятельность</a></li>
			</ul>
		</li>
		<li><a href="?route=studying">Учёба</a></li>
		<li><a href="?route=photos" class="now">Фотоальбом</a></li>
		<li><a href="?route=contacts">Контакты</a></li>
		<li><a href="?route=history">История</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
		<div id="gallery" class="gallery"></div>

		<div id="myModal" class="modal">
			<span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>
			<img class="modal-content" id="img01">
			<span class="next">&#9658;</span>
			<span class="prev">&#9668;</span>
			<div id="caption"></div>
		</div>
	<script src="/assets/js/scripts.js" type="text/javascript"></script>
	<script src="/assets/js/gallery.js" type="text/javascript"></script>
		<script>
			hobbyMenu();
			visitPage("photos");
			setCookie("photos");
		</script>
</body>
</html>