<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<script src = "/assets/js/scripts.js" type="text/javascript"></script>
	<script src = "/assets/js/home.js" type="text/javascript"></script>
	<script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
	<link rel=stylesheet type="text/css" href="/assets/css/style_main.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<title>Лабораторная работа №7</title>
</head>

<body>
<div id="nav">
	<ul>
	<li><a href="index.php" class="now">Главная</a></li>
	<li><a href="?route=aboutme">Обо мне</a></li>
	<li id="hobbyMenu" ><a>Интересы</a>
		<ul id="subHobbyMenu">
			<li><a href="?route=interests#hobby">Мои хобби</a></li>
			<li><a href="?route=interests#books">Любимые книги</a></li>
			<li><a href="?route=interests#music">Любимая музыка</a></li>
			<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
			<li><a href="?route=interests#films">Любимые фильмы</a></li>
			<li><a href="?route=interests#games">Любимые игры</a></li>
			<li><a href="?route=interests#rso">Общественная деятельность</a></li>
		</ul>
	</li>
	<li><a href="?route=studying">Учёба</a></li>
	<li><a href="?route=photos">Фотоальбом</a></li>
	<li><a href="?route=contacts">Контакты</a></li>
	<li><a href="?route=history">История</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
<div id="wrapper">
	<div class="image">
	<img src="/assets/img/me2.JPG"  height=450" width="380" border="3" alt="Это я.">
	</div>
	<div id="info">
		<h1>Гальшина Екатерина Алексеевна</h1>
		<h2>Группа ИС/б-32-о</h2>
		<h3>Лабораторная работа №7</h3>

	</div>

	<button id="myBtn">Нe жми на меня!</button>
	<div id="myModal" class="modal">
		<div class="modal-content">
			<span class="close">&#246</span>
			<h5>Спасибо, с Вашего счёта списано 100$ в пользу нашего сайта.</h5>
		</div>
	</div>

	<h3 align="center">
		<a align="center" href="?route=blog">Посетите мой блог!</a>
	</h3>

	<h3 align="center">
		<a align="center" href="?route=registration">Регистрация</a>
		/
		<a align="center" href="?route=index#autor">Вход</a>
	</h3>
	<br /><br /><br /><br /><br /><br /><br /><br /><br />

	<a name="autor"></a>
	<?php
	if ($_SESSION['vlogin'] != NULL) {
		echo "Welcome, ".$_SESSION['vlogin'];
		echo "<form align=\"center\" action=\"?route=autor/unautorizeVisitors\" method=\"post\">
                                    <input type=\"submit\" value=\"Выйти\" onClick=\"\"></form>";
	} else  {
		echo "<form align=\"center\" action=\"?route=autor/autorizeVisitors\" method=\"post\">
                                <input id=\"fio\" name=\"login\" class=\"inputContact\" type=\"text\" size=\"40\" placeholder=\"Логин\"><br>
                                <br/>
                                <input id =\"text\" type=\"text\" size=\"40\" name=\"pswd\" placeholder=\"Пароль\"><br>
                                <p><input type=\"submit\" value=\"Войти\" onClick=\"\">
                                <input type=\"reset\" value=\"Очистить\"></p></form></a>";
	}
	?>
</div>
	<div id="footer">
		<h3>SevGU, 2016</h3>
	</div>
</div>
<script>
	hobbyMenu();
	visitPage("index");
	setCookie("index");

	 var modal = document.getElementById('myModal');
	 var btn = document.getElementById("myBtn");
	 var span = document.getElementsByClassName("close")[0];


	 btn.onclick = function() {
	 modal.style.display = "block";
	 }

	 span.onclick = function() {
	 modal.style.display = "none";
	 }

	 window.onclick = function(event) {
	 if (event.target == modal) {
	 modal.style.display = "none";
	 }
	 }

</script>
</body>
</html>