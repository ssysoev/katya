<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<script src = "/assets/js/scripts.js" type="text/javascript"></script>
	<script src = "/assets/js/jquery.min.js" type="text/javascript"></script>
	<script src = "/assets/js/home.js" type="text/javascript"></script>
	<link rel=stylesheet type="text/css" href="/assets/css/style_main.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<title>Лабораторная работа №7</title>
</head>

<body>
<div id="nav">
	<ul>
		<li><a href="index.php" class="now">Главная</a></li>
		<li><a href="?route=aboutme">Обо мне</a></li>
		<li id="hobbyMenu" ><a>Интересы</a>
			<ul id="subHobbyMenu">
				<li><a href="?route=interests#hobby">Мои хобби</a></li>
				<li><a href="?route=interests#books">Любимые книги</a></li>
				<li><a href="?route=interests#music">Любимая музыка</a></li>
				<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
				<li><a href="?route=interests#films">Любимые фильмы</a></li>
				<li><a href="?route=interests#games">Любимые игры</a></li>
				<li><a href="?route=interests#rso">Общественная деятельность</a></li>
			</ul>
		</li>
		<li><a href="?route=studying">Учёба</a></li>
		<li><a href="?route=photos">Фотоальбом</a></li>
		<li><a href="?route=contacts">Контакты</a></li>
		<li><a href="?route=history">История</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
<div class="wrapper clearfix">
	<div id="info">
		<form action="?route=registration/send" method="post">

			<h3>Ваше ФИО:</h3>
			<p><input name="fio" id="name" class="inputContact" type="text" size="40" placeholder="Name Lastname Surname"><br></p>

			<h3>Ваш e-mail:</h3>
			<p><input name="email" id="email" class="inputContact" type="text" size="40"placeholder="E-mail"><br></p>

			<h3>Ваше Логин:</h3>
			<p>
				<input name="login" id="login" class="inputContact" type="text" size="40" placeholder="Name Lastname Surname">
				<div id="login-status"></div>
			</p>

			<h3>Ваше Пароль:</h3>
			<p><input name="pswd" id="pswd" class="inputContact" type="text" size="40" placeholder="Name Lastname Surname"><br></p>

			<p><input id="send" type="submit" class="inputContact" value="Отправить">
				<input type="reset"  class="inputContact" value="Очистить" onClick="clearInput()"></p>
		</form>
	</div>
</div>

	<div id="footer">
		<h3>SevGU, 2016</h3>
	</div>
<script src="/assets/js/ajax.js"></script>
<script>
	hobbyMenu();
</script>
</body>
</html>