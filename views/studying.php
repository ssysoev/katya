<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Лабораторная работа №7</title>
	<link rel=stylesheet type="text/css" href="/assets/css/style.css">
	<link rel=stylesheet type="text/css" href="/assets/css/menu.css">
	<script src="/assets/js/scripts.js" type="text/javascript"></script>
	<link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="nav">
	<ul>
		<li><a href="index.php">Главная</a></li>
		<li><a href="?route=aboutme">Обо мне</a></li>
		<li id="hobbyMenu" ><a>Интересы</a>
			<ul id="subHobbyMenu">
				<li><a href="?route=interests#hobby">Мои хобби</a></li>
				<li><a href="?route=interests#books">Любимые книги</a></li>
				<li><a href="?route=interests#music">Любимая музыка</a></li>
				<li><a href="?route=interests#studying">Любимые учебные предметы</a></li>
				<li><a href="?route=interests#films">Любимые фильмы</a></li>
				<li><a href="?route=interests#games">Любимые игры</a></li>
				<li><a href="?route=interests#rso">Общественная деятельность</a></li>
			</ul>
		</li>
		<li><a href="?route=studying" class="now">Учёба</a></li>
		<li><a href="?route=photos">Фотоальбом</a></li>
		<li><a href="?route=contacts">Контакты</a></li>
		<li><a href="?route=history">История</a></li>
	</ul>
	<h4 id="time"></h4>
	<script>
		setInterval(showDateAndTime, 100);
	</script>
</div>
<table frame="box" border="2" align="center" rules="all">
	<caption><b>Учебный план студентов ИС</b></caption>
	<tr>
		<th rowspan="3" width="5%">№</th>
		<th rowspan="3" width="60%">Дисциплина</th>
		<th colspan="12" width="35%">Часов в неделю <br>(лекций, лаб.раб., практ.раб.)</th>
	</tr>
	<tr>
		<th colspan="6">1 курс</th>
		<th colspan="6">2 курс</th>
	</tr>
	<tr>
		<th colspan="3">1 сем</th>
		<th colspan="3">2 сем</th>
		<th colspan="3">3 сем</th>
		<th colspan="3">4 сем</th>
	</tr>
	<tr>
		<td>1</td>
		<td>Экология</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td>1</td><td>0</td><td>1</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>2</td>
		<td>Высшая математика</td>
		<td>3</td><td>0</td><td>3</td><td>3</td><td>0</td><td>3</td><td>2</td><td>0</td><td>2</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>3</td>
		<td>Русский язык и культура речи</td>
		<td>1</td><td>0</td><td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>4</td>
		<td><a href="?route=test">Основы дискретной математики</a></td>
		<td>2</td><td>0</td><td>1</td><td>3</td><td>0</td><td>2</td><td></td><td></td><td></td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>5</td>
		<td>Основы программирования и алгоритмические языки</td>
		<td>3</td><td>2</td><td>0</td><td>3</td><td>3</td><td>0</td><td>0</td><td>0</td><td>1</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>6</td>
		<td>Основы экологии</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td>1</td><td>0</td><td>0</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>7</td>
		<td>Теория вероятностей и математическая статистика</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td>3</td><td>1</td><td>0</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>8</td>
		<td>Физика</td>
		<td>2</td><td>2</td><td>0</td><td>2</td><td>2</td><td>0</td><td>2</td><td>1</td><td>0</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>9</td>
		<td>Основы электродинамики и электроники</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td>2</td><td>1</td><td>1</td><td></td><td></td><td></td>
	</tr>
	<tr>
		<td>10</td>
		<td>Численные методы в информатике</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td>2</td><td>2</td><td>0</td><td>0</td><td>0</td><td>1</td>
	</tr>
	<tr>
		<td>11</td>
		<td>Методы исследования операций</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td>1</td><td>1</td><td>0</td><td>2</td><td>1</td><td class="celli">1</td>
	</tr>
</table>

<h3 align="center">
	<a align="center" href="?route=testresult">Посмотреть результаты прохождения теста!</a>
</h3>
<script>
	hobbyMenu();
	visitPage("studying");
	setCookie("studying");
</script>
</body>
</html>