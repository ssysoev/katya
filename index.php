<?php
/**
 * Created by PhpStorm.
 * User: katya
 * Date: 25.05.16
 * Time: 21:05
 */
session_start();
error_reporting (E_ALL);
require_once "config.php";
require_once "./core/init.php";

$db = new Database($registry);
$registry->set('db', $db);
$db->create();


$template = new Template($registry);

$registry->set ('template', $template);

$router = new Router($registry);
$registry->set('router', $router);
$router->setPath(SITE_PATH . 'controllers');
$router->delegate();



